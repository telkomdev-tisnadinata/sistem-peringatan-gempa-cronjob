var cron = require('node-cron');
var rp = require('request-promise');

const nodeList1 = [
  {
    node_name: "node-1-1",
    earthquake_strength: 0
  },{
    node_name: "node-1-2",
    earthquake_strength: 0
  },{
    node_name: "node-1-3",
    earthquake_strength: 0
  },{
    node_name: "node-1-4",
    earthquake_strength: 0
  },{
    node_name: "node-1-5",
    earthquake_strength: 0
  }
];
const nodeList2 = [
  {
    node_name: "node-2-1",
    earthquake_strength: 0
  },{
    node_name: "node-2-2",
    earthquake_strength: 0
  },{
    node_name: "node-2-3",
    earthquake_strength: 0
  }
];
let numReq = 1;
cron.schedule('*/5 * * * * *', () => {
  const perangkat_1 = `http://localhost:8081/device/sinyal`;
  const perangkat_2 = `http://localhost:8082/device/sinyal`;
  // const perangkat_1 = `http://ta-gempa-perangkat-1.apps.playcourt.id/device/sinyal`;
  // const perangkat_2 = `http://ta-gempa-perangkat-2.apps.playcourt.id/device/sinyal`;
  console.log('');
  console.log('Request ke-'+numReq);
  sendData(1, perangkat_1, nodeList1);
  sendData(2, perangkat_2, nodeList2);
  numReq++;
});

async function sendData(perangkat, host, nodeList) {
  const result = nodeList.map(node => {
    node.earthquake_strength = (Math.random() * (0.0 - 8.0) + 8.0).toFixed(1);
    return node;
  });
  const requestTime = new Date().toISOString();
  const body = await Promise.all(result);
  var options = {
    method: 'PUT', uri: host+`?requestTime=${requestTime}`, body: body, json: true
  };
 
  rp(options)
    .then(function (parsedBody) {
      console.log('Perangkat '+perangkat+' : '+requestTime+' : success');
    })
    .catch(function (err) {
      console.log('Perangkat '+perangkat+' : '+requestTime+' : '+err);
    });
}
